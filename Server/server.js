const path = require("path");
const express = require("express");
const socketIO = require("socket.io");
// const http = require("http");
import http from "http";
const PORT = 3000;
// import "../node_modules/bootstrap/dist/css/bootstrap.min.css";

const pathFile = path.join(__dirname, "/../public");
const app = express();
const server = http.createServer(app);
let io = socketIO(server);

app.use(express.static(pathFile));

io.on("connection", (socket) => {
    console.log("new user connected");

    socket.emit("newMessage", {
        from: "Admin",
        text: "Welcome to chat app!",
        createdAAt: new Date().getTime(),
    });

    socket.broadcast.emit("newMessage", {
        from: "Admin",
        text: "New user joined!",
        createdAAt: new Date().getTime(),
    });

    socket.on("createMessage", (message) => {
        console.log("createMessage", message);

        io.emit("newMessage", {
            from: message.from,
            text: message.text,
            createdAt: new Date().getTime(),
        });
    });

    socket.on("disconnect", () => {
        console.log("user was disconnected");
    });
});

server.listen(PORT, () => {
    console.log(`server running on port num ${PORT}`);
});
